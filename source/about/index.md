---
layout: "about"
title: "About"
date: 2021-10-15 11:05:33
description: The man was lazy and left nothing
header-img: "img/header_img/tag_bg.jpg"
---
## Who Am I

> 我是 if，00后，19软件工程在读，热爱coding、音乐以及游戏

## Got To Say

这个博客里不会放太多很杂乱的博文，如有需要请移步[我的csdn](https://blog.csdn.net/jay_chou345)

本博客使用Hexo框架和LiveMyLife主题搭建，[Get Start](http://ifyyf.com/cn/Hexo-Theme-LiveMyLife/)



之前自己全栈写过一个博客，但是感觉并没有很好看，代码写的也不好，[ifblog](https://gitee.com/ifyyf/ifblog)

而且那个服务器也过期了，所以想了想还是换上了现在这套hexo博客



最后，与君共勉

> 今天应做的事没有做，明天再早也是耽误了。


## My Friends

> 自由编程协会：http://freeprogramming.cn/

## Contact me

> Email：ifyyf@qq.com
>
> WeChat：YYF346313208
>
> QQ：346313208

## Treat me to coffee

> Coffee is not necessary
> Make friends with me

