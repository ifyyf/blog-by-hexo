---
layout: "about"
title: "About"
date: 2021-10-15 11:05:33
description: The man was lazy and left nothing
header-img: "img/scenery/my-bg1.jpg"
---
## Who Am I

> 19软件工程在读学生

## Got To Say

> 没有100%的聪明，那就去付出200%的努力吧，加油！


## My Friends

> 自由编程协会：http://freeprogramming.cn/

## Contact me

> Email：ifyyf@qq.com
> QQ：346313208

## Treat me to coffee
> Coffee is not necessary
> Make friends with me

